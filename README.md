<!--[![Generic badge](https://img.shields.io/badge/<SUBJECT>-<STATUS>-<COLOR>.svg)](https://shields.io/) -->
![Maintainer](https://img.shields.io/badge/maintainer-FabianSperk-blue)

<h1 align="center" style="display: block; font-size: 2.5em; font-weight: bold; margin-block-start: 1em; margin-block-end: 1em;">
  <br><br><strong>BLE DistanceAlarm</strong>
</h1>

# Table of contents

1. [Basics about the project](#1-basics-about-the-project) <br>
1.1 [Intended Use](#11-intended-use) <br>
1.2 [Project State](#12-project-state) <br>
2. [Start work on this project](#2-start-work-on-this-project) <br>
    2.1 [Project Structure](#21-project-structure) <br>
    2.2 [what you need](#22-what-you-need) <br>
        2.2.1 [Software for Development](#221-software-for-development)<br>
        2.2.2 [Hardware for Development](#222-hardware-for-development)<br>
        2.2.3 [Software for Testing](#223-software-for-testing)<br>
        2.2.4 [Hardware for Testing](#224-hardware-for-testing)<br>
3. [How to get started](#3-how-to-get-started) <br>
4. [The application in depth](#4-the-application-in-depth) <br>
    4.1 [BLE Interface](#41-ble-interface) <br>
    4.2 [distance Sensor](#42-distance-sensor) <br>
    4.3 [alarm feedback](#43-alarm-feedback) <br>
    4.4 [a typical scenario](#44-a-typical-scenario) <br>


# 1. Basics about the project ![](./docs/img/pin.svg)

## 1.1 Intended Use
The application can be used to meassure the distance, and ring an alarm if the meassured distance falls below a preset value.
This can for example be used to monitor the waterlevel of a given river or watertank. 
Once installed the application is easy to use and configure.
- as the device can be configured via [BLE](#https://de.wikipedia.org/wiki/Bluetooth_Low_Energy), changes to the settings can be made with ease and on the fly.
  
![Example video](./docs/vids/Example.mp4)

## 1.2 Project State
- still in development
- working prototype but no finished application
- Still Missing:
  - Hardware design
  - OTA DFU capabilities would be nice


# 2. Start work on this project ![](./docs/img/pin.svg)

## 2.1 Project Structure
```
.
|-- BLE_Sensor           
    |-- components        
        |-- AlarmFeedback               #src and header files for AlarmFeedback
        |-- CustomService               #src and header files for CustomService    
        |-- DistanceSensor              #src and header files for DistanceSensor
        |-- bluetooth                   #src and header files for bluetooth
    |-- hex                             #executable
    |-- pca10040/s132
        |-- ses
            |-- BLE_Sensor.emProject    #Segger-Embedded-Studio project file
    |-- ble_app_template.eww            #not of concern
    |-- main.c                          #main file (application startpoint)
|-- SoftwareAndTools                    #folder containing the SDK
|-- README.md                           #readme file
```
---
---
## 2.2 what You need

### 2.2.1 Software for Development
- [Git](https://git-scm.com/downloads)
- [Embedded Studio by Segger](https://www.segger.com/products/development-tools/embedded-studio/)
### 2.2.2 Hardware for Development
- Development-Kit: [nRF52 DK](https://www.nordicsemi.com/Products/Development-hardware/nrf52-dk)
- Distance Sensor: [Sharp GP2Y0A02YK0F](https://www.sparkfun.com/products/8958)
---
### 2.2.3 Software for Testing
- [nRF Connect for Desktop](https://www.nordicsemi.com/Products/Development-tools/nRF-Connect-for-desktop)
### 2.2.4 Hardware for Testing
- BLE Sniffer: [nRf52840 Dongle](https://www.nordicsemi.com/Products/Development-hardware/nrf52840-dongle)
  
**NOTE** 💡 For testing the application, any mobile phone with the nRF App installed will also do.

---
---


## 3. How to get started ![](./docs/img/pin.svg)
1) make sure you have installed all the necessary software as stated under the [Software for Development](#software-for-development) section.
2) clone the repo to a local directory (ATTENTION: make sure to clone it close to [root](https://en.wikipedia.org/wiki/Root_directory#:~:text=In%20a%20computer%20file%20system,where%20all%20branches%20originate%20from.))
```
cd <dir-of-your-choice>
git init
git clone <link to this repo>
```
3) open the project in Segger Embedded Studio
- go to ble_sensor\BLE_Sensor\pca10040\s132\ses
- double click on BLE_Sensor.emProject

4) add/remove code; recompile and flash to the device
   
# 4. The application in depth
## 4.1 BLE Interface
The user can connect to the application via [BLE](#https://de.wikipedia.org/wiki/Bluetooth_Low_Energy) either with his mobile phone, or via the [nRF Connect for Desktop](#223-software-for-testing) App  and discover the available services and characteristics.
- The application advertises as "PeeRadar"

The proprietary service for our application holds the following characteristics:
|Characteristic Name| Access | Properties| Usage|
| :- | :-: | :- | :-|
| SensorReading|r| notify| can be used to stream life sensordata.<br/> Intedned to be used in order to set the *AlarmDistance* characteristic<br/> 1) stream Data and determine appropriate distance<br/>  2) write this distance to *AlarmDistance*
|AlamrDistance| rw | -| any Object that comes within this range triggers an alarm (after AlarmDelay seconds have passed)
|AlarmDelay| rw | -| Delay after which the alarm is triggered
|AlarmState| rw |- | Used to active (1) and deactivate the alarm (0)

## 4.2 distance Sensor
- [SHARP GP2Y0A02YK0F](#https://www.sparkfun.com/datasheets/Sensors/Infrared/gp2y0a02yk_e.pdf)
## 4.3 Alarm Feedback
- in the current implementation LED 1 lights up if the alarm is activated.
## 4.4 A typical scenario
1) The user installs the device inside a watertank or besides a river.
2) The user connects to the device via BLE and configures it to his needs.

```plantuml
@startuml
    Title: Setting up the Device
    skinparam backgroundColor #EEEBDC
    skinparam handwritten flase
    actor User
    User -> "Device" : pressed Button 1
    "Device" -> "GATT Server" : start advertising
    activate "GATT Server"
    "Device" <- "GATT Server" : Fast advertising for 10[s]; Name: PeeRadar
    User -> "GATT Server" : establish connection
    User -> "GATT Server" : subscribe to SensorReading characterisic (notify)
    User <- "GATT Server" : stream Sensor Data
    User -> "GATT Server" : unsubscribe from SensorReading characterisic
    User -> "GATT Server" : write appropriat distance to AlarmDistance characterisic
    "GATT Server" -> "Device" : update AlarmDistance
    User -> "GATT Server" : write appropriat delay to AlarmDelay characterisic
    "GATT Server" -> "Device" : update AlarmDelay
    User -> "GATT Server" : write 1 to AlarmState characterisic to enable the alarm
    "GATT Server" -> "Device" : enable alarm
    User -> "GATT Server" : disconnect
    deactivate "GATT Server"
@enduml
```



