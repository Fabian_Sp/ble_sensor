#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "nordic_common.h"
#include "boards.h"
#include "nrf_delay.h"

/*------ START LOGGER includes -----*/
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
/*------ END LOGGER includes -----*/

/**
 * @brief Function for application main entry.
 */
int main(void)
{

/*------ START LOGGER setup -----*/
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL)); //init and error check
    NRF_LOG_DEFAULT_BACKENDS_INIT();
/*------ END LOGGER setup -----*/


    while (true)
    {
/*------ START LOGGER USECASE -----*/
      uint32_t count = 42;
      NRF_LOG_INFO("This is log data"); //print message and put INFO label behind it
      NRF_LOG_INFO("THIS is value of c: %d",count);
      nrf_delay_ms(500);
      count++;
/*------ END LOGGER USECASE -----*/
        // Do nothing.
    }
}
/** @} */
