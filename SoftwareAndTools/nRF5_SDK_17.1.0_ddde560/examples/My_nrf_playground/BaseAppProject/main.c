

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/** ++++++++++++++++ NRF LOGGER Pt43B ++++++++++++++++ 
-> sdk_config:
    NRF_LOG_ENABLED 1 -> further control defines are possible:
      NRF_LOG_DEFAULT_LEVEL ->dependatn on level longer/shorter messages
      NRF_LOG_DEFFERED 1->transmitt data beforre going to sleep
    select uart or rtt
    NRF_LOG_BACKEND_UART_ENABLE 1 or NRF_LOG_BACKEND_RTT_ENABLE 1
    ->also possible to set buff sicze or baudrate
**/
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


/** ++++++++++++++++ Timers Init Pt43C ++++++++++++++++ **/
#include "app_timer.h"

/** ++++++++++++++++ BSP Pt43D ++++++++++++++++ **/
#include "bsp_btn_ble.h"

/** ++++++++++++++++ PM Pt43E ++++++++++++++++ **/
//go into sleep mode if no action is needed ->SD has control over MCU powermodes
#include "nrf_pwr_mgmt.h"

/** ++++++++++++++++ SD Pt43F ++++++++++++++++ **/
//check sdk_config.h if SD is enabled
//NRF_SDH_BLE_ENABLED 1
//NRF_SDH_ENABLED 1 ->here are further settings (Clock ->XTAL for external, or interal clk source.. furthermore the accuracy and so on must be set..)
//also check Flash placement in linker! (theres an own tutorial)
//furthermore the Loader must be checked to see that it also loads the SD (min 5:00)
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
//queued writer ->needed for connection handle
#include "nrf_ble_qwr.h"
//create quequed writer instance -> QWR for sinlge connection; QWRS when multiple connection are to be supported
NRF_BLE_QWR_DEF(m_qwr);


/** ++++++++++++++++ GAP Pt43G ++++++++++++++++ **/
#define DEVICE_NAME           "Fabian Dev"
 //function makes sure we get propper ms units
#define MIN_CONN_INTERVAL     MSEC_TO_UNITS(100, UNIT_1_25_MS)
#define MAX_CONN_INTERVAL     MSEC_TO_UNITS(200, UNIT_1_25_MS)
#define SLAVE_LATENCY         0 //no of times the slave is allowed to skip a con-event
#define CONN_SUP_TIMEOUT      MSEC_TO_UNITS(2000, UNIT_10_MS) //max time between to successfull conn events

/** ++++++++++++++++ GATT Pt43H ++++++++++++++++ **/
#include "nrf_ble_gatt.h"
//create GATT instance
NRF_BLE_GATT_DEF(m_gatt); //GAtt instance called ->m_gatt



/** ++++++++++++++++ Advertisement Pt43I ++++++++++++++++ **/
#include "ble_advdata.h"
#include "ble_advertising.h"

#define APP_ADV_INTERVAL      300
#define APP_ADV_DURATION      0
//add avetr instance
BLE_ADVERTISING_DEF(m_advertising);


/** ++++++++++++++++ Service Init Pt43J ++++++++++++++++ **/



/** ++++++++++++++++ Conn params Init Pt43K ++++++++++++++++ **/
#define FIRST_CONN_PARAMS_UPDATE_DELAY      APP_TIMER_TICKS(5000)
#define NEX_CONN_PARAMS_UPDATE_DELAY       APP_TIMER_TICKS(30000)
#define MAX_CONN_PARAMS_UPDATE_COUNT        3

#include "ble_conn_params.h"
















/** ++++++++++++++++ NRF LOGGER Pt43B ++++++++++++++++ **/
static void log_init(){
  ret_code_t err_code = NRF_LOG_INIT(NULL);
  //APP_ERROR_CHECK(err_code);

  NRF_LOG_DEFAULT_BACKENDS_INIT();
 }


 /** ++++++++++++++++ Timers Init Pt43C ++++++++++++++++ **/
 //make sure APP_TIMER_ENABLE 1 (in sdk_config.h) ->further settings are possible..
 static void timers_init(void){
  ret_code_t err_code = app_timer_init();
  //APP_ERROR_CHECK(err_code);
  //even if we dont use app timers in our app, we still need to init them for the SD
 }

/** ++++++++++++++++ BSP Pt43D ++++++++++++++++ **/
static void leds_init(void){
  ret_code_t err_code = bsp_init(BSP_INIT_LEDS, NULL);
  //APP_ERROR_CHECK(err_code);
}

/** ++++++++++++++++ PM Pt43E ++++++++++++++++ **/
static void power_management_init(void){
  ret_code_t err_code = nrf_pwr_mgmt_init();
  //APP_ERROR_CHECK(err_code);
}

//function gets called previous to goint to sleep -> to output nrf logg messages
static void idle_state_handle(void){
  //make sure that the logger is finished for now (flushed all buffers)
  if(NRF_LOG_PROCESS() == false)
  {
  //only then run the power manager -> will set system to sleep
    nrf_pwr_mgmt_run();
   }
}


/** ++++++++++++++++ SD Pt43F ++++++++++++++++ **/
#define APP_BLE_CONN_CFG_TAG    1
#define APP_BLE_OBSERVER_PRIO   3 //NVIC priority for BLE events



static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;

static void ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context)
{
  ret_code_t err_code = NRF_SUCCESS;
  
  //see docu for all possible event cases
  switch(p_ble_evt->header.evt_id)
  {
    case BLE_GAP_EVT_DISCONNECTED:
      NRF_LOG_INFO("Device is disconnected\n");
    break;

    case BLE_GAP_EVT_CONNECTED:
      //indicate connection
      err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
      //APP_ERROR_CHECK(err_code);

      //connection handle to reffernce connection; assign handle to queued writer
      m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
      err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
      //APP_ERROR_CHECK(err_code);
      //debug message
      NRF_LOG_INFO("Device is connected\n");
    break;
  
    //handle update requerst from central ->if it request higher speeds e.g. PHY 1MB to 2MB
    case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
      //debug message
      NRF_LOG_INFO("Update PHY\n");
      ble_gap_phys_t const phys =
      {
        .rx_phys = BLE_GAP_PHY_AUTO,
        .tx_phys = BLE_GAP_PHY_AUTO,
      };
      err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
      //APP_ERROR_CHECK(err_code);
    break;

  }
}

//BLE stack init ->init SD
static void ble_stack_init(){
  ret_code_t err_code;

  //send enable request to check if we can enable the SD
  err_code = nrf_sdh_enable_request();
  //APP_ERROR_CHECK();

  uint32_t ram_start = 0;
  //configure default settings
  err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
  //APP_ERROR_CHECK(err_code);

  //enable SD
  err_code = nrf_sdh_ble_enable(&ram_start);
  //APP_ERROR_CHECK(err_code);


  //regsiter ble cb function
  NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);

}


/** ++++++++++++++++ GAP Pt43G ++++++++++++++++ **/
static void gap_params_init()
{
  ret_code_t err_code;

  ble_gap_conn_params_t     gap_conn_params;
  ble_gap_conn_sec_mode_t  sec_mode;

  //makro to fill struct with appropriate connten
  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode); //Security Mode 1 ->1. No Security

  //set device name
  err_code = sd_ble_gap_device_name_set(&sec_mode,(const uint8_t*)DEVICE_NAME, strlen(DEVICE_NAME));
  ////APP_ERROR_CHECK(err_code);

  //configure connetion parameters. ->clear struct and fill with our defined values
  memset(&gap_conn_params, 0, sizeof(gap_conn_params));
  gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
  gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
  gap_conn_params.slave_latency = SLAVE_LATENCY;
  gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

  //pass struct to function that will set the conn params
  err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
  ////APP_ERROR_CHECK(err_code);

}



/** ++++++++++++++++ GATT Pt43I ++++++++++++++++ **/
static void gatt_init(void){
  ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
  ////APP_ERROR_CHECK(err_code);
}


/** ++++++++++++++++ Advertisement Pt43I ++++++++++++++++ **/
//advertising event handler
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
  ret_code_t err_code;
  switch(ble_adv_evt)
  {
    //device enters fast adv state
    case BLE_ADV_EVT_FAST:
      NRF_LOG_INFO("Fast advertising...\n");
      err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
      APP_ERROR_CHECK(err_code);
      break;

    //no advert currently
    case BLE_ADV_EVT_IDLE:
     NRF_LOG_INFO("No advertising...\n");
     err_code = bsp_indication_set(BSP_INDICATE_IDLE);
     APP_ERROR_CHECK(err_code);
     break;

    default:
      break;

  }

}

static void advertising_init(void)
{
  ret_code_t err_code;
  //struct that holds adv values
  ble_advertising_init_t adv_cfg;
  memset(&adv_cfg, 0, sizeof(adv_cfg));

  adv_cfg.advdata.name_type = BLE_ADVDATA_FULL_NAME;
  adv_cfg.advdata.include_appearance = true;
  adv_cfg.advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
  adv_cfg.config.ble_adv_fast_enabled = true;
  adv_cfg.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
  adv_cfg.config.ble_adv_fast_timeout = APP_ADV_DURATION;

  adv_cfg.evt_handler = on_adv_evt;

  err_code = ble_advertising_init(&m_advertising, &adv_cfg);
  APP_ERROR_CHECK(err_code);

  ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);

}



/** ++++++++++++++++ Service Init Pt43J ++++++++++++++++ **/
//error handler for queue writer
static void nrf_qr_error_handler(uint32_t nrf_error){
  APP_ERROR_CHECK(nrf_error);

}
//init query writer model
static void services_init(void)
{
  ret_code_t err_code;

  nrf_ble_qwr_init_t qwr_init = {0};

  qwr_init.error_handler = nrf_qr_error_handler;

  err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
  APP_ERROR_CHECK(err_code);

}

/** ++++++++++++++++ Conn params Init Pt43K ++++++++++++++++ **/
//event hanler for conn params update
static void on_conn_params_evt(ble_conn_params_evt_t *p_evt)
{
  ret_code_t err_code;

  //failed to update conn params
  if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
  {
  //terminate connection
    err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
    APP_ERROR_CHECK(err_code);
  }
  
  //updated conn params succesfully
  if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_SUCCEEDED)
  {
    
  }

}

//error handler for conn params update
static void conn_params_error_handler(uint32_t nrf_error)
{
  APP_ERROR_HANDLER(nrf_error);
}

//function willl init conn params
static void conn_params_init(void)
{
  ret_code_t err_code;

  ble_conn_params_init_t cp_init;
  memset(&cp_init, 0, sizeof(cp_init));

  cp_init.p_conn_params = NULL;
  //send first request, if nothing happens send next request after an even longer time
  cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
  cp_init.next_conn_params_update_delay = NEX_CONN_PARAMS_UPDATE_DELAY;
  cp_init.max_conn_params_update_count = MAX_CONN_PARAMS_UPDATE_COUNT; //if the connection is not updated after 3 times ->issue error

  cp_init.start_on_notify_cccd_handle = BLE_GATT_HANDLE_INVALID;

  cp_init.disconnect_on_fail = false; //dont disconnect if the update failed

  cp_init.error_handler = conn_params_error_handler;

  cp_init.evt_handler = on_conn_params_evt;

  err_code = ble_conn_params_init(&cp_init);
  APP_ERROR_CHECK(err_code);

}



/*LAST STEP*/
static void advertising_start(void){

  ret_code_t err_code;

  //look at adv init function and start advertising the was you initialized it
  //e.g fast advertising mode!
  err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
  APP_ERROR_CHECK(err_code);

}





/**@brief Function for application main entry.
 */
int main(void)
{
    log_init();
    timers_init();
    leds_init();
    power_management_init();
    ble_stack_init();
    gap_params_init();
    gatt_init();
    advertising_init();
    services_init();
    conn_params_init();
    NRF_LOG_INFO("BLE Base appliction started...\n");

    advertising_start(); //make visible


    // Enter main loop.
    for (;;)
    {
      idle_state_handle();//turn over control to the pwr-mgmt
    }
}


/**
 * @}
 */
