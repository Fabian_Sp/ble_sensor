
#include "my_libs.h"

//LED and Button numbers  -> see bottom of PCB

uint32_t FS_ReadInput(uint32_t PinNumber)
{
  if(nrf_gpio_pin_read(PinNumber) == 0) //button pressed -> pin is pulled low
  {
    return 1;
  }
  return 0;
}

void FS_light_led(uint32_t PinNumber)
{
  printf("LED SET\r\n");
  nrf_gpio_pin_set(PinNumber);
}

void FS_clear_led(uint32_t PinNumber)
{
  nrf_gpio_pin_clear(PinNumber);
}
void run_led(void)
{
      nrf_gpio_pin_set(17);
      nrf_delay_ms(500);
      nrf_gpio_pin_clear(17);
      nrf_delay_ms(500);
}