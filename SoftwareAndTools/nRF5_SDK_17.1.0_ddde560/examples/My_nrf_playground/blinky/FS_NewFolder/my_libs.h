#ifndef _FS_MY_
#define _FS_MY_

#include "nrf_delay.h"
#include "nrf_gpio.h"

void FS_light_led(uint32_t PinNumber);
void FS_clear_led(uint32_t PinNumber);

uint32_t FS_ReadInput(uint32_t PinNumber);

void run_led(void);

#endif