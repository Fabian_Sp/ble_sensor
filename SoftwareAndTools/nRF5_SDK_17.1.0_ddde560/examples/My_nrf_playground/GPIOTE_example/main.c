
#include <stdbool.h>
#include "nrf.h"
#include "nrf_drv_gpiote.h"
#include "app_error.h"
#include "boards.h"



void cb_in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    nrf_gpio_pin_toggle(LED_1);
}
/**
 * @brief Function for configuring: PIN_IN pin for input, PIN_OUT pin for output,
 * and configures GPIOTE to give an interrupt on pin change.
 */
static void gpio_init(void)
{
    ret_code_t err_code;

    //set led 1 as output
    nrf_gpio_cfg_output(LED_1);
    nrf_gpio_pin_set(LED_1);

    //init general gpiote and check retval in debug mode
    err_code = nrf_drv_gpiote_init();
    APP_ERROR_CHECK(err_code);

    //struct to configure input ->detect a high to low transistion
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
    in_config.pull = NRF_GPIO_PIN_PULLUP;

    //pass pin, config-stuct and cb-function to setup GPIOTE
    err_code = nrf_drv_gpiote_in_init(BUTTON_1, &in_config, cb_in_pin_handler);
    err_code = 400;
    APP_ERROR_CHECK(err_code);

    //enable the configured GPIOTE
    nrf_drv_gpiote_in_event_enable(BUTTON_1, true);
}

/**
 * @brief Function for application main entry.
 */
int main(void)
{
    gpio_init();

    while (true)
    {
        // Do nothing.
    }
}


/** @} */
