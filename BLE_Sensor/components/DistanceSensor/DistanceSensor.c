
#include "DistanceSensor.h"


/**
 * @brief Not used in this example, but driver API requiers a callback function to be proivded.
 */
static void saadc_callback(nrf_drv_saadc_evt_t const * p_event);



ret_code_t DistanceSensor_init(void){
    NRF_LOG_DEBUG("DistanceSensor_init()");
   ret_code_t err_code;
   nrf_saadc_channel_config_t channel_config =
   NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);

   err_code = nrf_drv_saadc_init(NULL, saadc_callback);
   APP_ERROR_CHECK(err_code);

   err_code = nrf_drv_saadc_channel_init(SAADC_CHANNEL, &channel_config);
   APP_ERROR_CHECK(err_code);
   return err_code;
}


ret_code_t DistanceSensor_getReading(uint16_t *reading){
    ret_code_t err_code;

    if(reading == NULL)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    nrf_saadc_value_t sample;
    NRF_LOG_DEBUG("DistanceSensor_getReading()");
    err_code = nrfx_saadc_sample_convert(SAADC_CHANNEL, &sample);
    APP_ERROR_CHECK(err_code);
    NRF_LOG_DEBUG("Sample: %i", sample);


    *reading = sample;
    return err_code;
}


ret_code_t DistanceSensor_getReading_cm(uint16_t *reading){
    ret_code_t err_code;

    if(reading == NULL)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    nrf_saadc_value_t sample;
    NRF_LOG_DEBUG("DistanceSensor_getReading_cm()");
    err_code = nrfx_saadc_sample_convert(SAADC_CHANNEL, &sample);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEBUG("Sample raw: %i", sample);

    if(sample <= 89)
    {
        *reading = 150;
    }else if(sample > 89 && sample <= 93)
    {
        *reading = 140;
    }else if(sample > 93 && sample <= 102)
    {
        *reading = 130;        
    }else if(sample > 102 && sample <= 113)
    {
        *reading = 120;       
    }else if(sample > 113 && sample <= 123)
    {
        *reading = 110;        
    }else if(sample > 123 && sample <= 135)
    {
        *reading = 100;       
    }else if(sample > 135 && sample <= 143)
    {
        *reading = 90;       
    }else if(sample > 143 && sample <= 168)
    {
        *reading = 80; 
    }else if(sample > 168 && sample <= 185)
    {
        *reading = 70; 
    }else if(sample > 185 && sample <= 215)
    {
        *reading = 60; 
    }else if(sample > 215 && sample <= 254)
    {
        *reading = 50; 
    }else if(sample > 254 && sample <= 318)
    {
        *reading = 40; 
    }else if(sample > 318 && sample <= 410)
    {
        *reading = 30; 
    }else if(sample > 410 && sample <= 512)
    {
        *reading = 20; 
    }else if(sample > 512 && sample <= 574)
    {
        *reading = 15; 
    }else // sample > 574
    {
      *reading = 15;
    }

    NRF_LOG_DEBUG("Sample in cm: %i", *reading);


    
    return err_code;
}


static void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
}

/** @} */