#ifndef FS_DISTANCE_SENSOR
#define FS_DISTANCE_SENSOR


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "sdk_errors.h"
#include "stdlib.h"
#include "nrf.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "boards.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "app_util_platform.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


#include "nrf_drv_twi.h"




/**< SAADC Channel used to get analog sensor reading */
#define SAADC_CHANNEL 0

/**
 * @brief Function for confguring SAADC channel 0 for sampling AIN0 (Analog Input 0)
 *        on pin 0.02 (P0.02) in SingleEnded OneShot mode. For mor Saadc settings see
 *        app_config.h and NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE macro expansion
 *        Used to retain analog input from Sensor
 * 
 * @return ret_code_t 
 */
ret_code_t DistanceSensor_init(void);

/**
 * @brief get new reading from ADC Channel previously configured in DistanceSensor_init()
 * 
 * @param[out] reading  Sensor Reading as unsigned 16-bit value
 * @return ret_code_t 
 */
ret_code_t DistanceSensor_getReading(uint16_t *reading);

/**
 * @brief function to convert sensor Reading from V to cm
 * 
 * @attention function assumes the following conditions:
 *            -> SAADC module samples with 10-bit mode
 *            -> Sensor is GP2Y0A02YK0F --> "20150" [20cm to 150cm]
 *            -> values under 20cm are error readings that are not identified
 *            -> follwing conversion table is used:
 *                      Volt    Distance    ADC reading(at 5V range with 10bit resolution)
                        2,8	    15          ~574
                        2,5	    20          512
                        2	    30          ~410
                        1,55	40          ~318
                        1,24	50          ~254
                        1,05	60          ~215
                        0,905	70          ~185
                        0,82	80          ~168
                        0,7	    90          ~143
                        0,66	100         ~135
                        0,6	    110         ~123
                        0,55	120         ~113
                        0,5	    130         ~102
                        0,455	140         ~93
                        0,435	150         ~89
 * @param reading  Distance Reading in cm
 * @return ret_code_t 
 */
ret_code_t DistanceSensor_getReading_cm(uint16_t *reading);







#endif //FS_DISTANCE_SENSOR