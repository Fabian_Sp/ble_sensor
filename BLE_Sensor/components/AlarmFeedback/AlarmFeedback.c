#include "AlarmFeedback.h"



ret_code_t AlarmFeedback_init(void){
    ret_code_t err_code;
    nrf_gpio_cfg_output(ALARM_FEEDBACK_GPIO_PIN); // Initialize the Led 
    nrf_gpio_pin_set(ALARM_FEEDBACK_GPIO_PIN); /**< assumes that LEDs are sinked and not sourced via the IC*/
    return err_code;
}


ret_code_t AlarmFeedback_startAlarm(void){
    ret_code_t err_code;
    nrf_gpio_pin_clear(ALARM_FEEDBACK_GPIO_PIN); /**< assumes that LEDs are sinked and not sourced via the IC*/
    return err_code;
}


ret_code_t AlarmFeedback_stopAlarm(void){
    ret_code_t err_code;
    nrf_gpio_pin_set(ALARM_FEEDBACK_GPIO_PIN); /**< assumes that LEDs are sinked and not sourced via the IC*/
    return err_code;
}