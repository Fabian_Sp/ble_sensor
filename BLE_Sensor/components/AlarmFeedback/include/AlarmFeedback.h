#ifndef FS_ALARM_FEEDBACK
#define FS_ALARM_FEEDBACK

#include "sdk_errors.h"
#include "stdint.h"
#include "stdlib.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/**< pin used to output alarm signal */
#define ALARM_FEEDBACK_GPIO_PIN 17


/**
 * @brief initialize whaterver functionality that is needed to ring an alarm
 *        e.b: GPIO, Timers, PWN,...
 * 
 * @return ret_code_t 
 */
ret_code_t AlarmFeedback_init(void);


/**
 * @brief function to start the alarm
 *
 * @return ret_code_t 
 */
ret_code_t AlarmFeedback_startAlarm(void);


/**
 * @brief function to stop the alarm
 * 
 * @return ret_code_t 
 */
ret_code_t AlarmFeedback_stopAlarm(void);




#endif //FS_ALARM_FEEDBACK