#include "CustomService.h"

/**< register an observer for our Service with the SoftDevice*/
BLE_PEERADAR_DEF(m_peeRadar);

/**< register Timer instance -> used for SensoReading char notify intervall*/
APP_TIMER_DEF(m_SensorReading_char_timer_id);
#define SENSORREADING_NOTIFY_INTERVAL_MS 1000
#define SENSORREADING_NOTIFY_INTERVAL_TIMER_TICKS  APP_TIMER_TICKS(SENSORREADING_NOTIFY_INTERVAL_MS)

/**< register Timer instance -> used for cyclic DistanceSensor reading */
APP_TIMER_DEF(m_GetSensorReading_timer_id);
#define GETSENSORREADING_INTERVAL_MS 200
#define GETSENSORREADING_INTERVAL_TIMER_TICKS  APP_TIMER_TICKS(GETSENSORREADING_INTERVAL_MS) // 500 ms intervals

/**< handle to store connection information accros functions of this file*/
static uint16_t g_conn_handle  = BLE_CONN_HANDLE_INVALID;


/**
 * @brief add sensor Reading characterisitc to service
 * 
 * @param[in,out] p_blink acts as config struct as well as a way to retain information retained
 *                from the SoftDevice upon adding the Characterisitc to the service through
 *                the SoftDevice 
 * @param context set initial value of characteristic
 * @return ret_code_t 
 */
static ret_code_t add_sensorReading_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context);

/**
 * @brief add alarm Distance characterisitc to service
 * 
 * @param[in,out] p_blink acts as config struct as well as a way to retain information retained
 *                from the SoftDevice upon adding the Characterisitc to the service through
 *                the SoftDevice 
 * @param context set initial value of characteristic
 * @return ret_code_t 
 */
static ret_code_t add_alarmDistance_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context);

/**
 * @brief add alarm Delay characterisitc to service
 * 
 * @param[in,out] p_blink acts as config struct as well as a way to retain information retained
 *                from the SoftDevice upon adding the Characterisitc to the service through
 *                the SoftDevice 
 * @param context set initial value of characteristic
 * @return ret_code_t 
 */
static ret_code_t add_AlarmDelay_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context);

/**
 * @brief add alarm State characterisitc to service
 * 
 * @param[in,out] p_blink acts as config struct as well as a way to retain information retained
 *                from the SoftDevice upon adding the Characterisitc to the service through
 *                the SoftDevice 
 * @param context set initial value of characteristic
 * @return ret_code_t 
 */
static ret_code_t add_alarmState_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context);

/**
 * @brief Function for handling characteric write events
 * 
 * @param[in] p_ble_evt   Event received from the BLE stack.
 * @param[in] p_peeRadar  struct that holds all relevant data for the service and all its characteristics
 */
static void on_write(ble_evt_t const *p_ble_evt, ble_peeRadar_t *p_peeRadar);

/**
 * @brief function is called on write event commng from AlarmDistance Chararacteruítics value
 * 
 * @param[out] p_peeRadar  structure used to safe inforamtion about service and characteristic accross
 *                        function calls. Inside this function the settings.AlarmDistance member gets updated
 * @param[in] data        pointer to data that was writen to AlamrDistance Characteristics value field
 */

static void on_AlarmDistance_write_evt(ble_peeRadar_t *p_peeRadar, uint8_t const *data);
/**
 * @brief function is called on write event commng from AlarmDelay Chararacteruítics value
 * 
 * @param[out] p_peeRadar  structure used to safe inforamtion about service and characteristic accross
 *                        function calls. Inside this function the settings.AlarmDelay member gets updated
 * @param[in] data        pointer to data that was writen to AlarmDelay Characteristics value field
 */
static void on_AlarmDelay_write_evt(ble_peeRadar_t *p_peeRadar, uint8_t const *data);
/**
 * @brief function is called on write event commng from AlarmState Chararacteruítics value
 * 
 * @param[out] p_peeRadar  structure used to safe inforamtion about service and characteristic accross
 *                        function calls. Inside this function the settings.AlarmState member gets updated
 * @param[in] data        pointer to data that was writen to AlarmState Characteristics value field
 */
static void on_AlarmState_write_evt(ble_peeRadar_t *p_peeRadar, uint8_t const *data);


/**
 * @brief function toggles subscribed member of p_peeRadar, and dependatn on its state start or stops app timer
 *        to send out periodic SensorReadings
 *        function is called on write event comming from SensorReading Chararacteruítics cccd field
 * 
 * @param[out] p_peeRadar  structure used to safe inforamtion about service and characteristic accross
 *                        function calls. Inside this function the settings.AlarmState member gets updated
 */
static void on_SensorReading_cccd_notify_subscription(ble_peeRadar_t *p_peeRadar);

/*
  PLACEHOLDER exchagne with sensor reading form ADC/I2c/SPI/..
*/
void SensorReading_characteristic_update(uint16_t conn_handle, uint16_t value_handle, uint16_t *temperature);


/**
 * @brief function is called upon SensorReading timer timeout event
 * 
 * @param p_context  unsued paramter
 */
static void SensorReading_char_timer_timeout_handler(void * p_context);

/**
 * @brief funciton is called upon GetSensorReading timer timeout event
 * 
 * @param p_context 
 */
static void GetSensorReading_timer_timeout_handler(void * p_context);



ret_code_t peeRadar_init(peeRadar_settings_t *p_context)
{
  ret_code_t err_code;
  ble_uuid_t ble_uuid;
  ble_uuid128_t base_uuid = {PEERADAR_UUID_BASE};

  if(p_context == NULL)
  {
    return NRF_ERROR_INVALID_PARAM;
  }

  /*initialize resources for DistanceSensor*/
  err_code = DistanceSensor_init();
  VERIFY_SUCCESS(err_code);
  
  /*initialize resources needed to perfomr alarm */
  err_code = AlarmFeedback_init();


  /*add the service*/
  err_code = sd_ble_uuid_vs_add(&base_uuid, &m_peeRadar.service_type);
  VERIFY_SUCCESS(err_code);

  ble_uuid.type = m_peeRadar.service_type;
  ble_uuid.uuid = PEERADAR_UUID_SERVICE;
  err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &m_peeRadar.service_handle);
  VERIFY_SUCCESS(err_code);


  p_context->AlarmDelay_s = 10;
  p_context->AlarmDistance_cm = 60;
  p_context->AlarmState = 00;
  /*add characteristics */
  err_code = add_sensorReading_char(&m_peeRadar, p_context);
    VERIFY_SUCCESS(err_code);
  err_code = add_alarmDistance_char(&m_peeRadar, p_context);
    VERIFY_SUCCESS(err_code);
  err_code = add_AlarmDelay_char(&m_peeRadar, p_context);
    VERIFY_SUCCESS(err_code);
  err_code = add_alarmState_char(&m_peeRadar, p_context);
    VERIFY_SUCCESS(err_code);
#if CALLBACK_MECHANISM_IN_USE
  m_peeRadar.sensorReading_char_writeHandler = on_sensorReading_char_evt;
  m_peeRadar.alarmDistance_char_writeHandler = on_alarmDistance_char_evt;
#endif
  return err_code;
}



void peeRadar_ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context){
  ble_peeRadar_t *p_peeRadar = (ble_peeRadar_t *)p_context;

          if (!p_ble_evt || !p_peeRadar)
                  return;

          switch (p_ble_evt->header.evt_id)
          {
          case BLE_GAP_EVT_CONNECTED:
            g_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            /* stop cyclic sensor Readings as soon as user connects to the device
               alarm wont sound, saves energy */
            app_timer_stop(m_GetSensorReading_timer_id);
                  break;
          case BLE_GAP_EVT_DISCONNECTED:
            g_conn_handle = BLE_CONN_HANDLE_INVALID;
            /* in case user disconnects while still being subscribed to 
               chararcteristic notify ->stop notify timer too safe energy */
            if(p_peeRadar->subscribed == SUBSCRIBED_YES) 
            {
              p_peeRadar->subscribed = SUBSCRIBED_NO;
              app_timer_stop(m_SensorReading_char_timer_id);
            }
            /*user disconnected and set alarm active -> start cyclic sensor
              readings and ring alarm in case someone pees while standing*/
            if(p_peeRadar->settings.AlarmState == 1)
            {
              app_timer_start(m_GetSensorReading_timer_id, GETSENSORREADING_INTERVAL_TIMER_TICKS, p_peeRadar);
            }
                  break;
          case BLE_GATTS_EVT_WRITE:
          {
            on_write(p_ble_evt, (ble_peeRadar_t*)p_context);
            break;
          }
          default:
                  return;
          }
}



static ret_code_t add_sensorReading_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context)
{
    ret_code_t err_code;
    ble_add_char_params_t add_char_params;
    ble_add_char_user_desc_t add_char_user_desc;
    static char *user_desc_text = "Sensor Reading";

    /* Add the SensorReading characteristic */
    memset(&add_char_params, 0, sizeof add_char_params);  //clean struct
    add_char_params.uuid = PEERADAR_UUID_CHARACTERISTIC_SENSOR_READING;  //provide char UUID
    add_char_params.uuid_type = p_blink->service_type;    //provide this chars service UUID as received by the SoftDevice
    add_char_params.init_len = sizeof(m_peeRadar.distanceSensorReading);           //set initial char value len
    add_char_params.max_len = sizeof(m_peeRadar.distanceSensorReading);            //set max char value len
    //add_char_params.p_init_value = m_peeRadar.distanceSensorReading;
    add_char_params.char_props.read = 0;                  //disable reading from char
    add_char_params.char_props.write = 0;                 //disable writing to char
    
    
  

    add_char_params.cccd_write_access = 1;            //enbale user to write to cccd
    add_char_params.char_props.notify = 1;            //notify

    add_char_params.read_access = SEC_OPEN;               //sucurity paramters open = no security level required to read or write value
    add_char_params.write_access = SEC_OPEN;

    /*charactereistic user description*/
    memset(&add_char_user_desc, 0, sizeof add_char_user_desc);
    add_char_user_desc.max_size = strlen(user_desc_text);
    add_char_user_desc.size = strlen(user_desc_text);
    add_char_user_desc.p_char_user_desc = (uint8_t *)user_desc_text;
    add_char_user_desc.is_var_len = false;
    add_char_user_desc.read_access = SEC_OPEN;            //only readable, not writeable
    add_char_user_desc.write_access = SEC_NO_ACCESS;
    //add user description to general char struct
    add_char_params.p_user_descr = &add_char_user_desc;

    //register char with BLE Stack
    err_code = characteristic_add(p_blink->service_handle,
                                  &add_char_params,
                                  &p_blink->sensorReading_char_handle);
    VERIFY_SUCCESS(err_code);

    // create but not dont start app timer yet
    app_timer_create(&m_SensorReading_char_timer_id, APP_TIMER_MODE_REPEATED, SensorReading_char_timer_timeout_handler);
    app_timer_create(&m_GetSensorReading_timer_id, APP_TIMER_MODE_REPEATED, GetSensorReading_timer_timeout_handler);

    return err_code;
}



static ret_code_t add_alarmDistance_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context)
{
    peeRadar_settings_t *settings = (peeRadar_settings_t*)context;
    ret_code_t err_code;
    ble_add_char_params_t add_char_params;
    ble_add_char_user_desc_t add_char_user_desc;
    static char *user_desc_text = "Alarm Distance";

    /* Add the SensorReading characteristic */
    memset(&add_char_params, 0, sizeof add_char_params);  //clean struct
    add_char_params.uuid = PEERADAR_UUID_CHARACTERISTIC_ALARM_DISTANCE;  //provide char UUID
    add_char_params.uuid_type = p_blink->service_type;    //provide this chars service UUID
    add_char_params.init_len = sizeof(settings->AlarmDistance_cm);           //set initial char value len
    add_char_params.max_len = sizeof(settings->AlarmDistance_cm);            //set max char value len
    //add_char_params.p_init_value = (uint8_t*)settings->AlarmDistance_cm;
    add_char_params.char_props.read = 1;                  //enbale reading from char
    add_char_params.char_props.write = 1;                 //enbale writing to char

    add_char_params.read_access = SEC_OPEN;               //sucurity paramters open = no security level required to read or write value
    add_char_params.write_access = SEC_OPEN;

    /*charactereistic user description*/
    memset(&add_char_user_desc, 0, sizeof add_char_user_desc);
    add_char_user_desc.max_size = strlen(user_desc_text);
    add_char_user_desc.size = strlen(user_desc_text);
    add_char_user_desc.p_char_user_desc = (uint8_t *)user_desc_text;
    add_char_user_desc.is_var_len = false;
    add_char_user_desc.read_access = SEC_OPEN;            //only readable, not writeable
    add_char_user_desc.write_access = SEC_NO_ACCESS;
    //add user description to general char struct
    add_char_params.p_user_descr = &add_char_user_desc;

    //register char with BLE Stack
    err_code = characteristic_add(p_blink->service_handle,
                                  &add_char_params,
                                  &p_blink->alarmDistance_char_handle);
    VERIFY_SUCCESS(err_code);
    //p_blink->sensorReading_char_evtHandler = context->
    //p_blink->led_ena_wr_handler = p_blink_init->led_ena_wr_handler;

    return err_code;
}


static ret_code_t add_AlarmDelay_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context)
{
    peeRadar_settings_t *settings = (peeRadar_settings_t*)context;
    ret_code_t err_code;
    ble_add_char_params_t add_char_params;
    ble_add_char_user_desc_t add_char_user_desc;
    static char *user_desc_text = "Alarm Delay";

    /* Add the SensorReading characteristic */
    memset(&add_char_params, 0, sizeof add_char_params);  //clean struct
    add_char_params.uuid = PEERADAR_UUID_CHARACTERISTIC_ALARM_DELAY;  //provide char UUID
    add_char_params.uuid_type = p_blink->service_type;    //provide this chars service UUID
    add_char_params.init_len = sizeof(settings->AlarmDelay_s);           //set initial char value len
    add_char_params.max_len = sizeof(settings->AlarmDelay_s);            //set max char value len
    add_char_params.p_init_value = &settings->AlarmDelay_s;
    add_char_params.char_props.read = 1;                  //enbale reading from char
    add_char_params.char_props.write = 1;                 //enbale writing to char


    add_char_params.read_access = SEC_OPEN;               //sucurity paramters open = no security level required to read or write value
    add_char_params.write_access = SEC_OPEN;

    /*charactereistic user description*/
    memset(&add_char_user_desc, 0, sizeof add_char_user_desc);
    add_char_user_desc.max_size = strlen(user_desc_text);
    add_char_user_desc.size = strlen(user_desc_text);
    add_char_user_desc.p_char_user_desc = (uint8_t *)user_desc_text;
    add_char_user_desc.is_var_len = false;
    add_char_user_desc.read_access = SEC_OPEN;            //only readable, not writeable
    add_char_user_desc.write_access = SEC_NO_ACCESS;
    //add user description to general char struct
    add_char_params.p_user_descr = &add_char_user_desc;

    //register char with BLE Stack
    err_code = characteristic_add(p_blink->service_handle,
                                  &add_char_params,
                                  &p_blink->AlarmDelay_char_handle);
    VERIFY_SUCCESS(err_code);
    //p_blink->sensorReading_char_evtHandler = context->
    //p_blink->led_ena_wr_handler = p_blink_init->led_ena_wr_handler;

    return err_code;
}

static ret_code_t add_alarmState_char(ble_peeRadar_t *p_blink, peeRadar_settings_t *context)
{
    peeRadar_settings_t *settings = (peeRadar_settings_t*)context;
    ret_code_t err_code;
    ble_add_char_params_t add_char_params;
    ble_add_char_user_desc_t add_char_user_desc;
    static char *user_desc_text = "Alarm State";

    /* Add the SensorReading characteristic */
    memset(&add_char_params, 0, sizeof add_char_params);  //clean struct
    add_char_params.uuid = PEERADAR_UUID_CHARACTERISTIC_ALARM_STATE;  //provide char UUID
    add_char_params.uuid_type = p_blink->service_type;    //provide this chars service UUID
    add_char_params.init_len = sizeof(settings->AlarmState);           //set initial char value len
    add_char_params.max_len = sizeof(settings->AlarmState);            //set max char value len
    add_char_params.p_init_value = &settings->AlarmState;
    add_char_params.char_props.read = 1;                  //enbale reading from char
    add_char_params.char_props.write = 1;                 //enbale writing to char

    add_char_params.read_access = SEC_OPEN;               //security paramters open = no security level required to read or write value
    add_char_params.write_access = SEC_OPEN;

    /*charactereistic user description*/
    memset(&add_char_user_desc, 0, sizeof add_char_user_desc);
    add_char_user_desc.max_size = strlen(user_desc_text);
    add_char_user_desc.size = strlen(user_desc_text);
    add_char_user_desc.p_char_user_desc = (uint8_t *)user_desc_text;
    add_char_user_desc.is_var_len = false;
    add_char_user_desc.read_access = SEC_OPEN;            //only readable, not writeable
    add_char_user_desc.write_access = SEC_NO_ACCESS;
    //add user description to general char struct
    add_char_params.p_user_descr = &add_char_user_desc;

    //register char with BLE Stack
    err_code = characteristic_add(p_blink->service_handle,
                                  &add_char_params,
                                  &p_blink->alarmState_char_handle);
    VERIFY_SUCCESS(err_code);
    return err_code;
}


static void on_write(ble_evt_t const *p_ble_evt, ble_peeRadar_t *p_peeRadar)
{
    ble_gatts_evt_write_t const *p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;


    if(p_evt_write->handle == p_peeRadar->sensorReading_char_handle.cccd_handle)
    {
      on_SensorReading_cccd_notify_subscription(p_peeRadar);
    }
    if(p_evt_write->handle == p_peeRadar->alarmDistance_char_handle.value_handle)
    {
      on_AlarmDistance_write_evt(p_peeRadar, p_evt_write->data);
    }
    if(p_evt_write->handle == p_peeRadar->AlarmDelay_char_handle.value_handle)
    {
      on_AlarmDelay_write_evt(p_peeRadar, p_evt_write->data);
    }
    if(p_evt_write->handle == p_peeRadar->alarmState_char_handle.value_handle)
    {
      on_AlarmState_write_evt(p_peeRadar, p_evt_write->data);
    }
}


static void on_AlarmDistance_write_evt(ble_peeRadar_t *p_peeRadar, uint8_t const *data)
{
  NRF_LOG_INFO("on_AlarmDistance_write_evt() distance value: %d",*data);
  p_peeRadar->settings.AlarmDistance_cm = *data;
}

static void on_AlarmDelay_write_evt(ble_peeRadar_t *p_peeRadar, uint8_t const *data)
{
  NRF_LOG_INFO("on_AlarmDelay_write_evt() delay value: %d",*data);
  p_peeRadar->settings.AlarmDelay_s = *data;
}

static void on_AlarmState_write_evt(ble_peeRadar_t *p_peeRadar, uint8_t const *data)
{
  NRF_LOG_INFO("on_AlarmState_write_evt() alamrstate value: %d",*data);
  p_peeRadar->settings.AlarmState = *data;
}


static void on_SensorReading_cccd_notify_subscription(ble_peeRadar_t *p_peeRadar)
{
    NRF_LOG_INFO("on_SensorReading_cccd_notify_subscription()");

    //toggle subsription flag
    p_peeRadar->subscribed = ((p_peeRadar->subscribed == SUBSCRIBED_YES) ? SUBSCRIBED_NO : SUBSCRIBED_YES);
    if(p_peeRadar->subscribed == SUBSCRIBED_YES)
      {
        app_timer_start(m_SensorReading_char_timer_id, SENSORREADING_NOTIFY_INTERVAL_TIMER_TICKS, NULL);
      }
      else{ 
         app_timer_stop(m_SensorReading_char_timer_id);
      }
}

static void SensorReading_char_timer_timeout_handler(void * p_context)
{
  UNUSED_PARAMETER(p_context);
  NRF_LOG_DEBUG("SensorReading_char_timer_timeout_handler() ");
  DistanceSensor_getReading_cm(&m_peeRadar.distanceSensorReading);
  SensorReading_characteristic_update(g_conn_handle, m_peeRadar.sensorReading_char_handle.value_handle, &m_peeRadar.distanceSensorReading);

}



void SensorReading_characteristic_update(uint16_t conn_handle, uint16_t value_handle, uint16_t *temperature){
  ret_code_t err_code;
  uint8_t help[2];
  help[1] = (*temperature & 0xFF);
  help[0] = (*temperature >> 8);
  if (conn_handle != BLE_CONN_HANDLE_INVALID)
  {
    ble_gatts_hvx_params_t hvx_params;
    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = value_handle;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    hvx_params.offset = 0;
    uint16_t len = sizeof(help);
    hvx_params.p_len  = &len;
    hvx_params.p_data = (uint8_t*)help;  
   
    sd_ble_gatts_hvx(conn_handle, &hvx_params);

    APP_ERROR_CHECK(err_code);

  }
  else{
  NRF_LOG_ERROR("conn handle invalid");
  }
  }

  static void GetSensorReading_timer_timeout_handler(void * p_context)
  {
  
    ble_peeRadar_t *p_peeRadar = (ble_peeRadar_t *)p_context;

    static int ObjectWithinRangeCount = 0;
    static uint8_t alarmFlag = 0; /** int round would trigger mltiple alarm issues; flag is used to avoid this*/

   
    uint16_t sensorValue = 0;
    DistanceSensor_getReading_cm(&sensorValue);
    if(sensorValue <= p_peeRadar->settings.AlarmDistance_cm)
    {
        ObjectWithinRangeCount++;
        NRF_LOG_DEBUG("GetSensorReading_timer_timeout_handler() WITHIN RANGE");
    }
    else{
      ObjectWithinRangeCount = 0;
      NRF_LOG_DEBUG("GetSensorReading_timer_timeout_handler() OUT OF RANGE");
      if(alarmFlag == 1)
      {
        NRF_LOG_INFO("ALARM stoped");
        AlarmFeedback_stopAlarm();
        alarmFlag = 0;
      }
    }
    
    if(ObjectWithinRangeCount/(1000/GETSENSORREADING_INTERVAL_MS) == p_peeRadar->settings.AlarmDelay_s && !alarmFlag)
    {
      alarmFlag = 1;
      NRF_LOG_INFO("ALARM started");
      AlarmFeedback_startAlarm();
    }
  }