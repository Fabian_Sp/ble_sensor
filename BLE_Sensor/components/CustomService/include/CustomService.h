#ifndef FS_PEESERVICE
#define FS_PEESERVICE

#include "stdint.h"
#include "bluetooth.h"
#include "AlarmFeedback.h"
#include "DistanceSensor.h"

/**< priority for Service instance that is to be regsiterd with the SoftDevice*/
#define BLE_PEERADAR_OBSERVER_PRIO  2

/** in case a later implementation needs to register callbacks on charcteristic 
 *  events all neccessary code was left in but commented out */
#define CALLBACK_MECHANISM_IN_USE  0


/**
 * @brief We define one base UUID (custom vendor specific UUID) that is 128-bit long
 *        FOr our sercice and the charactersisitcs of this service we generate the respective
 *        UUIDs by changing byte 3 and 4 of the base UUID.
 *        Service UUID = 0000-1969-4241-0381-2536-9412-FD7A-5541
 *        Char 1 UUID = 0000-196A-4241-0381-2536-9412-FD7A-5541
 *        Char 2 UUID = 0000-196B-4241-0381-2536-9412-FD7A-5541
 *        Char 3 UUID = 0000-196C-4241-0381-2536-9412-FD7A-5541
 *        Char 4 UUID = 0000-196D-4241-0381-2536-9412-FD7A-5541
 */
// 128-bit vendor-specific(custom) Service BASE UUID
#define PEERADAR_UUID_BASE {0x41, 0x55, 0x7A, 0xFD, 0x12, 0x94, 0x36, 0x25,\
                            0x81, 0x03, 0x41, 0x42, 0x00, 0x00, 0x00, 0x00}
#define PEERADAR_UUID_SERVICE 0x1969
#define PEERADAR_UUID_CHARACTERISTIC_SENSOR_READING 0x196A
#define PEERADAR_UUID_CHARACTERISTIC_ALARM_DISTANCE 0x196B
#define PEERADAR_UUID_CHARACTERISTIC_ALARM_DELAY 0x196C
#define PEERADAR_UUID_CHARACTERISTIC_ALARM_STATE 0x196D



/**
 * @brief makro to declare static variable and register a BLE observer with the SoftDevice
 * @note Macro does the following:
 *  - declare a filestatic user-defined object which retains and caches any information
 *  - registers a BLE Observer with the SoftDevice
 *  -> identifieable by its name
 *  -> with priotity as set by the BLE_PEERADAR_OBSERVER_PRIO define
 *  -> a callbackhandler type associated with it
 *  -> register the previously declared static varibale for use inside the callback handler
 */
#define BLE_PEERADAR_DEF(_name)\
  static ble_peeRadar_t _name;\
  NRF_SDH_BLE_OBSERVER(_name ## _obs,\
  BLE_PEERADAR_OBSERVER_PRIO,\
  peeRadar_ble_evt_handler, &_name)

/**
 * @brief forward declariton of ble_peeRadar_s struct for it to be used
 *        in decalration of service ble evtent handler function pointer typedef
 * 
 */
typedef struct ble_peeRadar_s ble_peeRadar_t;

#if CALLBACK_MECHANISM_IN_USE
/**
 * @brief function pointer typedef for ble events origination from characterisitcs of the custom service
 * 
 * @param[in] conn_handle connection handle associated with peer that envoced the characterics event
 * @param[in] p_blink     not used in current implementation, but might be usefull in later iterations
 * @param[in] data        used for passing data received from the characteristics via a write event to the
 *                        callback handler
 */
typedef void (*peeRadar_ble_evt_handler_t)(uint16_t conn_handle, ble_peeRadar_t *p_blink, uint8_t data);

#endif
/**
 * @brief structure to hold alarm cinfiguration information
 *        -> alarm rings if someone is within the configured alarm distance
 *           for a time longer than the configured delay
 * 
 */
typedef struct{
  uint8_t AlarmDistance_cm;  /**< distance at which the alarm should ring*/
  uint8_t AlarmDelay_s;     /**< delay after which the alarm should ring*/
  uint8_t AlarmState;     /**< state of the alarm: active/inactiv*/
}peeRadar_settings_t;

/**
 * @brief enum to be used in conjunction with cccd notify
 * 
 */
typedef enum{
  SUBSCRIBED_NO,
  SUBSCRIBED_YES,
} subscribeInfo_e;


/**
 * @brief A structure to:
 *        1) save all relevant data for the custom PeeRadar service as retained from the BLE Stack(SoftDevice)
 *        2) act as init config struct for servie initialisation (to register callback functions
 *           to characteristic events)
 */
struct ble_peeRadar_s{
  uint16_t service_handle; /**< cache Service handle assigned by the BLE stack */
  uint8_t  service_type;   /**< cache Service UUID type assigned by the BLE stack */
  ble_gatts_char_handles_t sensorReading_char_handle; /**< cache Handle for SensorReading characteristic */
  subscribeInfo_e subscribed; /**< used for SensorReadin char cccp notify*/
  ble_gatts_char_handles_t alarmDistance_char_handle; /**< cache Handle for AlarmDistance characteristic */
  ble_gatts_char_handles_t AlarmDelay_char_handle; /**< cache Handle for AlarmDelay characteristic */
  ble_gatts_char_handles_t alarmState_char_handle; /**< cache Handle for AlarmState characteristic */
  peeRadar_settings_t settings; /**< settings for the peeRadar ->at which distanc should alarm ring, etc. */
  uint16_t distanceSensorReading; /**< store last read value from distance sensor */
#if CALLBACK_MECHANISM_IN_USE
  peeRadar_ble_evt_handler_t sensorReading_char_writeHandler; /**< function pointer for callback 
                                                                   registration to sensorReading chararcteristic write event*/
  peeRadar_ble_evt_handler_t alarmDistance_char_writeHandler; /**< function pointer for callback 
                                                                   registration to alarmDistance chararcteristic write event*/
  peeRadar_ble_evt_handler_t AlarmDelay_char_writeHandler;    /**< function pointer for callback 
                                                                   registration to alarmDistance chararcteristic write event*/
#endif
};


#if CALLBACK_MECHANISM_IN_USE
/**
 * @brief Eventhanlder for events origination for the PeeRadar service.
 *        Function cannot be file-static, as it has to be registered with the SoftDevice.
 *        We also need to declare it in the header in order for the BLE_PEERADAR_DEF() macro to work.
 * 
 * @param p_ble_evt ble_evt_t type struct containing information relevenat to the event, filled by the SoftDevice
 * @param p_context conext struct -> in our case it can be casted to ble_PeeRadar_t type as we are accessing the
 *                  previously with the SoftDevice registered file-static variable (done via the BLE_PEERADAR_DEF() 
 *                  macro)
 */
#endif
void peeRadar_ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context);

/**
 * @brief function for initializing the PeeRadar Service
 * 
 * @param[in,out] p_peeRadar has 2 functions:
 *                   1) act as storage location for parameters recieved from the BLE 
 *                      stack upon serivice registration
 *                   2) allow for the registration of callbacks to events at the
 *                      characteristics of the service
 * @param[in]     p_context set initial values for characterisitcs
 * @return ret_code_t 
 */
ret_code_t peeRadar_init(peeRadar_settings_t *p_context);


void our_temperature_characteristic_update(uint16_t conn_handle, uint16_t value_handle, uint32_t *temperature);

#endif //FS_PEESERVICE