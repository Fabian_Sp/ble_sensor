#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "fds.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_gpiote.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "bluetooth.h"

#include "AlarmFeedback.h"
#include "DistanceSensor.h"


/**
 * @brief Function for initializing the nrf log module.
 */
static void log_init(void);

/**
 * @brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void);

/**
 * @brief Function for initializing power management.
 */
static void power_management_init(void);

/**
 * @brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation -> sleep until next the next event occurs.
 */
static void idle_state_handle(void);



/**
 * @brief Function for application main entry.
 */
int main(void)
{

    log_init();
    timers_init();
    power_management_init();

    bluetooth_init();

    // Start execution.
    NRF_LOG_INFO("Template example started.");
   // application_timers_start();

    advertising_start();

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}



static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

static void timers_init(void)
{
    // Initialize timer module.
    ret_code_t err_code = app_timer_init();
   // NRF_LOG_INFO("Error code: %d\n",err_code);
    APP_ERROR_CHECK(err_code);
}

static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}
