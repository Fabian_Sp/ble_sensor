  #ifndef APP_CONFIG_H
  #define APP_CONFIG_H

  //activate configurations here instead of the sdk_config.h
  
  /*Peer Manager Enabled*/
  // <e> PEER_MANAGER_ENABLED - peer_manager - Peer Manager
  //==========================================================
  #ifndef PEER_MANAGER_ENABLED
  #define PEER_MANAGER_ENABLED 1
  #endif

  /*Power Management Enabled*/
  // <e> NRF_PWR_MGMT_ENABLED - nrf_pwr_mgmt - Power management module
  //==========================================================
  #ifndef NRF_PWR_MGMT_ENABLED
  #define NRF_PWR_MGMT_ENABLED 1
  #endif

  // <q> NRF_PWR_MGMT_CONFIG_FPU_SUPPORT_ENABLED  - Enables FPU event cleaning.
  #ifndef NRF_PWR_MGMT_CONFIG_FPU_SUPPORT_ENABLED
  #define NRF_PWR_MGMT_CONFIG_FPU_SUPPORT_ENABLED 1
  #endif

  // <o> NRF_PWR_MGMT_CONFIG_HANDLER_PRIORITY_COUNT - The number of priorities for module handlers. 
  // <i> The number of stages of the shutdown process.
  #ifndef NRF_PWR_MGMT_CONFIG_HANDLER_PRIORITY_COUNT
  #define NRF_PWR_MGMT_CONFIG_HANDLER_PRIORITY_COUNT 3
  #endif


  /* LOGGER SETTINGS*/
  // 1 = ERROR
  // 2 = WARNING
  // 3 = INFO
  // 4 = DEBUG
  #define NRF_LOG_DEFAULT_LEVEL 3


  //==========================================================
  // <q> BUTTON_ENABLED  - Enables Button module
  #ifndef BUTTON_ENABLED
  #define BUTTON_ENABLED 1
  #endif


  //==========================================================
  // <q> BLE_ADVERTISING_ENABLED  - ble_advertising - Advertising module
  #ifndef BLE_ADVERTISING_ENABLED
  #define BLE_ADVERTISING_ENABLED 1
  #endif

  // <o> NRF_SDH_BLE_VS_UUID_COUNT - The number of vendor-specific UUIDs. 
  #ifndef NRF_SDH_BLE_VS_UUID_COUNT
  #define NRF_SDH_BLE_VS_UUID_COUNT 1
  #endif

  /**
   * GPIO pins used in this project
   */
  //#define ALARM_PIN 17

  /**
  *   USED for DistanceSensor Interfacing ->I2C
  
  #define NRFX_TWIM_ENABLED 1
  #define NRFX_TWI_ENABLED 1
  #define TWI_ENABLED 1
  #define TWI0_ENABLED 1
  //#define TWI0_USE_EASY_DMA 1
  */

  /**> USED for DistanceSensor Interfacing ->Analog*/
  /**>also requires: nrfx_saadc.c and nrff_saadc.h ->include & link applicable*/
  // enbale SAADC module
  #define SAADC_ENABLED 1
  // <o> SAADC_CONFIG_RESOLUTION
  // <0=> 8 bit 
  // <1=> 10 bit 
  // <2=> 12 bit 
  // <3=> 14 bit 
  #define SAADC_CONFIG_RESOLUTION 1
  // <o> SAADC_CONFIG_OVERSAMPLE
  // <0=> Disabled 
  // for more settings see sdk_config.h
  #define SAADC_CONFIG_OVERSAMPLE 0
  // <q> SAADC_CONFIG_LP_MODE
  // <0=> Disabled
  #define SAADC_CONFIG_LP_MODE 0
  // <o> SAADC_CONFIG_IRQ_PRIORITY  - Interrupt priority
  // <i> Priorities 0,2 (nRF51) and 0,1,4,5 (nRF52) are reserved for SoftDevice
  // <0=> 0 (highest) 
  // <1=> 1 
  // <2=> 2 
  // <3=> 3 
  // <4=> 4 
  // <5=> 5 
  // <6=> 6 
  // <7=> 7 
  #define SAADC_CONFIG_IRQ_PRIORITY 6

  #endif //#ifndef APP_CONFIG_H
